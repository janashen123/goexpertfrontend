import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import stateAsyncStatus from 'types/stateAsyncStatus';
import type { RootState } from '../index';
import generateMeeting from '../../services/meetings';
import { IBookInfo } from '../../types/IMeeting';

interface InitialState {
  status: stateAsyncStatus;
  error: null | string | unknown;
  data: Number | null;
}

const initialState: InitialState = {
  status: stateAsyncStatus.idle,
  error: null,
  data: null,
};

export const createMeeting = createAsyncThunk(
  'meeting/createMeeting',
  async (meetingInfo: IBookInfo) => {
    const data = await generateMeeting(meetingInfo);
    return data;
  },
);

const meetingSlice = createSlice({
  name: 'meeting',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        createMeeting.pending,
        (state: InitialState) => {
          state.status = stateAsyncStatus.loading;
        },
      )
      .addCase(
        createMeeting.fulfilled,
        (state: InitialState, action) => {
          state.status = stateAsyncStatus.succeeded;
          state.data = action.payload.status;
        },
      )
      .addCase(
        createMeeting.rejected,
        (state: InitialState, action) => {
          state.status = stateAsyncStatus.failed;
          state.error = action.payload;
        },
      );
  },
});

export const createMeetingStatus = (state: RootState) => state.meeting.status;

export default meetingSlice.reducer;
