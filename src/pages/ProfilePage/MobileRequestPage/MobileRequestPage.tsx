import React, { useEffect, useState } from 'react';
import { getMeetingByUserId, getMeetingByExpertId } from 'services';
import { useSelector } from 'react-redux';
import CloseIcon from '@mui/icons-material/Close';
import { Divider, IconButton } from '@mui/material';
import { get as storageGet } from 'utils/localStorage';
import { TOKEN } from 'constants/localStorageKeys';
import IParams from 'services/profile';
import { IMyRequest, IRecievedRequest } from 'types/IProfileRequest';
import { PROFILE_SETTINGS_MENU, PROFILE_SETTINGS_SUB_MENU } from 'constants/profileMenus';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import RequestCardMobile from './components/RequestCard';
import ReceivedCardMobile from './components/ReceivedCard';
import styles from './MobileRequestPage.module.scss';
import { selectCurrentUserID } from '../../../store/slices/userSlice';

interface IMobileRequestPageProps {
  active: string;
}

const MobileRequestPage: React.FC<IMobileRequestPageProps> = (props) => {
  const { active } = props;
  const userID = useSelector(selectCurrentUserID);
  const token = JSON.parse(storageGet(TOKEN));
  const [myRequests, setMyRequests] = useState<IMyRequest[]>([]);
  const [receivedRequests, setReceivedRequests] = useState<IRecievedRequest[]>([]);
  const [isFilterVisible, setIsFilterVisible] = useState(false);
  const [subActive, setSubActive] = useState(PROFILE_SETTINGS_SUB_MENU[0]);

  const myRequestParams = {
    userID,
    token,
  } as IParams;

  const filterData = (setFunc, myRequestData, condition) => {
    setFunc(myRequestData.filter(
      (item) => (item.status === condition),
    ));
  };

  useEffect(() => {
    switch (active) {
      case PROFILE_SETTINGS_MENU[1]:
        getMeetingByExpertId(myRequestParams)
          .then((data) => {
            const receivedRequestData = data as IRecievedRequest[];
            setReceivedRequests(receivedRequestData);
          })
          .catch((error) => error.response);
        break;
      default:
        getMeetingByUserId(myRequestParams)
          .then((data) => {
            const myRequestData = data as IMyRequest[];
            setMyRequests(myRequestData);
          })
          .catch((error) => error.response);
        break;
    }
  }, []);

  const handleClick = (clickedItem) => {
    setIsFilterVisible(false);
    setSubActive(clickedItem);
    switch (active) {
      case PROFILE_SETTINGS_MENU[1]:
        getMeetingByExpertId(myRequestParams)
          .then((data) => {
            const receivedRequestData = data as IRecievedRequest[];
            switch (clickedItem) {
              case PROFILE_SETTINGS_SUB_MENU[1]:
                filterData(setReceivedRequests, receivedRequestData, PROFILE_SETTINGS_SUB_MENU[1]);
                break;
              case PROFILE_SETTINGS_SUB_MENU[2]:
                filterData(setReceivedRequests, receivedRequestData, PROFILE_SETTINGS_SUB_MENU[2]);
                break;
              case PROFILE_SETTINGS_SUB_MENU[3]:
                filterData(setReceivedRequests, receivedRequestData, PROFILE_SETTINGS_SUB_MENU[3]);
                break;
              default:
                setReceivedRequests(receivedRequestData);
                break;
            }
          })
          .catch((error) => error.response);
        break;
      default:
        getMeetingByUserId(myRequestParams)
          .then((data) => {
            const myRequestData = data as IMyRequest[];
            switch (clickedItem) {
              case PROFILE_SETTINGS_SUB_MENU[1]:
                filterData(setMyRequests, myRequestData, PROFILE_SETTINGS_SUB_MENU[1]);
                break;
              case PROFILE_SETTINGS_SUB_MENU[2]:
                filterData(setMyRequests, myRequestData, PROFILE_SETTINGS_SUB_MENU[2]);
                break;
              case PROFILE_SETTINGS_SUB_MENU[3]:
                filterData(setMyRequests, myRequestData, PROFILE_SETTINGS_SUB_MENU[3]);
                break;
              default:
                setMyRequests(myRequestData);
                break;
            }
          })
          .catch((error) => error.response);
        break;
    }
  };

  return (
    <>
      <div className={styles.container}>
        {active !== PROFILE_SETTINGS_MENU[2] && (
          <div className={styles.header}>
            <p className={styles.subtitle}>{subActive}</p>
            <div className={styles.filter}>
              <FilterAltIcon />
              <button onClick={() => setIsFilterVisible(true)}>Filter by</button>
            </div>
          </div>
        )}
        {active === PROFILE_SETTINGS_MENU[0] && myRequests[0]
          && myRequests.map((myRequest) => (
            <RequestCardMobile myRequest={myRequest} />
          ))}
        {active === PROFILE_SETTINGS_MENU[1] && receivedRequests[0]
          && receivedRequests.map((receivedRequest) => (
            <ReceivedCardMobile receivedRequest={receivedRequest} />
          ))}
      </div>

      {isFilterVisible && (
        <div className={styles.filterMenu}>
          <div className={styles.topWrapper}>
            <h2 className={styles.title}>
              Filter by
            </h2>
            <IconButton className={styles.iconClose} onClick={() => setIsFilterVisible(false)}>
              <CloseIcon />
            </IconButton>
          </div>
          {PROFILE_SETTINGS_SUB_MENU.map((item) => (
            <div key={item}>
              <button type="button" className={styles.subMenu} onClick={() => handleClick(item)}>
                <p>{item}</p>
              </button>
              <Divider className={styles.divider} />
            </div>
          ))}
        </div>
      )}
    </>
  );
};

export default MobileRequestPage;
