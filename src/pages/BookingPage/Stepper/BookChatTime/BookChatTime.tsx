import React, { MouseEventHandler, useState } from 'react';
import {
  TextField,
  Box,
  Button,
  Radio,
  RadioGroup,
  FormControlLabel,
  Alert,
} from '@mui/material';
import { DesktopDatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { IBookInfo } from 'types/IMeeting';
import styles from './bookChatTime.module.scss';

interface BookCustomerInfoProps {
  handlePrevious: MouseEventHandler<HTMLButtonElement>;
  handleTimeSelect: Function;
  handleNext: Function;
  handleChange: Function;
  bookDetail: IBookInfo;
}

const time = [
  '10:00 - 11:30',
  '12:00 - 13:30',
  '16:00 - 17:30',
  '18:00 - 19:30',
  '19:30 - 21:00',
];

const BookChatTime: React.FC<BookCustomerInfoProps> = ({
  handlePrevious, handleNext, bookDetail, handleChange, handleTimeSelect,
}) => {
  const date = bookDetail?.date;
  const meetingTime = bookDetail?.time;
  const [errMsg, setErrMsg] = useState('');

  const handleValidation = () => {
    if (!meetingTime) {
      return setErrMsg('Please select a time!');
    }
    return handleNext();
  };

  return (
    <div className={styles.wrapper}>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <h2 className={styles.heading}>Choose Chatting Time</h2>
        <DesktopDatePicker
          value={date}
          minDate={new Date()}
          onChange={handleChange('date')}
          renderInput={(params) => <TextField {...params} />}
        />
        <Box className={styles.timeOptionContainer}>
          <RadioGroup value={meetingTime} onChange={handleTimeSelect('time')}>
            { time.map((item) => (
              <FormControlLabel value={item} key={item} control={<Radio />} label={item} />
            ))}
          </RadioGroup>
        </Box>
        {errMsg ? <Alert severity="error">{errMsg}</Alert> : <></>}
        <Box className={styles.buttonWrapper}>
          <Button className={styles.previousButton} onClick={handlePrevious}>Previous</Button>
          <Button className={styles.nextButton} onClick={handleValidation}>Next</Button>
        </Box>
      </LocalizationProvider>
    </div>
  );
};

export default BookChatTime;
