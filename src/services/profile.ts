import request from 'utils/request';
import { AxiosRequestConfig } from 'axios';

export default interface IParams {
  userID: string;
  token: string;
}

export const getMeetingByExpertId = (params: IParams) => {
  const { userID, token } = params;
  const config: AxiosRequestConfig = {
    url: `/meetings/experts/${userID}`,
    method: 'GET',
    headers: {
      authorization: token,
    },
  };
  return request(config).then((res) => res.data);
};

export const getMeetingByUserId = (params: IParams) => {
  const { userID, token } = params;
  const config: AxiosRequestConfig = {
    url: `/meetings/users/${userID}`,
    method: 'GET',
    headers: {
      authorization: token,
    },
  };
  return request(config).then((res) => res.data);
};

/**
 * get image upload url form server
 * @returns uploadURL, Key
 */
export const getImageUploadUrl = async () => {
  const config: AxiosRequestConfig = {
    url: '/users/profile/image-url',
    method: 'GET',
  };
  return request(config).then((res) => res.data);
};

/**
 * put image to s3 bucket
 * @param imageUploadUrl the s3 bucket upload url
 * @param imageFile the file to be upload
 */
export const putImage = async (imageUploadUrl: string, imageFile: File) => {
  return fetch(imageUploadUrl, {
    method: 'PUT',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    body: imageFile,
  });
};

/**
 * send image key to server
 * @param Key image key
 */
export const saveImageKey = async (Key: string, token: string, userID: string) => {
  const config: AxiosRequestConfig = {
    url: `/users/profile/image/${userID}`,
    method: 'PUT',
    data: { Key },
    headers: {
      authorization: token,
    },
  };
  return request(config).then((res) => res.data);
};
